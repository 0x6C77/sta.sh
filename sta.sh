#!/bin/bash

if [ ! $1 ]; then
    echo "Usage: ./sta.sh \$path [\$stash]"
    exit 1
fi

# Check target file exists
if [ ! -e "$1" ]; then
    echo "File doesn't exist"
    exit 1
elif [ ! -f "$1" ]; then
    echo "Directories are not currently supported"
    exit 1
fi

# Get full path of target file
FILE=$(realpath $1)
FILENAME=$(basename $FILE)

# Change to .stash directory
cd "$(dirname "$0")"

# Check computers folder exists 
HOST=$(hostname -f)

if [[ ! -e $HOST ]]; then
    echo "Creating host directory $HOST.."
    mkdir $HOST
fi

# Change to correct directory
cd $HOST

if [ $2 ]; then
    if [[ ! -e $2 ]]; then
        echo "Creating config directory $2.."
        mkdir $2
    fi

    cd $2
fi

# Save current directory for later reference
STASH=$(pwd)

# Check we won't overwrite an already stashed file
if [ -e "$FILENAME" ]; then
    echo "Stashed file already exists"
    ACTION="Updated "
else
    # Link files
    echo "Linking $FILENAME.."
    ln $FILE $FILENAME >/dev/null

    if [ $? -ne 0 ]; then
        echo "Link failed"
        exit 1
    fi
    ACTION="Adding "
fi

# Change to .stash directory
cd "$(dirname "$0")"

# Create commit message
if [ $2 ]; then
    GITMSG="$2/$FILENAME@$HOST"
else
    GITMSG="$FILENAME@$HOST"
fi
git add $STASH/$FILENAME >/dev/null 2>&1
git commit -m "$ACTION$GITMSG" >/dev/null 2>&1
git push >/dev/null 2>&1

echo "Stashed file successfully"